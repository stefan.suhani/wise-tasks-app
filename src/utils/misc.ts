export const getRandomInt = (min: number, max: number) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
};

export const getRandomItemFromList = <T>(array: T[]) => {
  return array[getRandomInt(0, array.length)];
};

export const ellipsis = (text: string, length: number) => (text.length > length ? `${text.slice(0, length)}...` : text);
