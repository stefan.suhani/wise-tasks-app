import * as prismicH from '@prismicio/helpers';

import { generateFlightUrl, generateProjectUrl } from './urls';

export const getProjectPreviewData = (project) => ({
  title: prismicH.asText(project.data.title),
  description: prismicH.asText(project.data.preview_text),
  url: generateProjectUrl(project.uid),
  photo: project.data.preview_image.url,
});

export const getTeamMemberData = (teamMember) => ({
  name: prismicH.asText(teamMember.name),
  role: prismicH.asText(teamMember.role),
  description: prismicH.asText(teamMember.description),
  photo: teamMember.photo.url,
});

export const getFlightPreviewData = (flight) => ({
  title: prismicH.asText(flight.data.title),
  summary: prismicH.asText(flight.data.summary),
  sponsor: prismicH.asText(flight.data.sponsor),
  url: generateFlightUrl(flight.uid),
  flightNumber: flight.data.flight_number,
  countries: flight.data.locations.map((location) => prismicH.asText(location.country).trim()),
  cities: flight.data.locations.map((location) => prismicH.asText(location.city).trim()),
  hasDescription: prismicH.isFilled.richText(flight.data.description),
});

export const getFlightStatistics = (statistics) => ({
  distance: prismicH.asText(statistics.data.kilometers_flown),
  adults: prismicH.asText(statistics.data.adults_flown),
  children: prismicH.asText(statistics.data.kids_flown),
  funds: prismicH.asText(statistics.data.money_paid),
});

export const getStatisticData = (statistic) => ({
  value: prismicH.asText(statistic.value),
  description: prismicH.asText(statistic.description),
});

export const getChildStoryDataPreview = (story) => ({
  name: prismicH.asText(story.data.name),
  age: prismicH.asText(story.data.age),
  image: prismicH.asImageSrc(story.data.before),
  slug: story.uid,
});

export const getSponsorData = (sponsor) => {
  return {
    name: prismicH.asText(sponsor.name),
    website: prismicH.asLink(sponsor.website),
    tier: sponsor.tier,
    logo: prismicH.asImageSrc(sponsor.logo),
  };
};

export const getMomentData = (moment) => {
  return {
    title: prismicH.asText(moment.data.title),
    postUrl: prismicH.asText(moment.data.post_url),
    description: prismicH.asText(moment.data.description),
    type: moment.data.type,
    image: prismicH.asImageSrc(moment.data.image),
  };
};
