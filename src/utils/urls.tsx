export const generateProjectUrl = (slug: string) => `/proiecte/${slug}`;

export const generateFlightUrl = (slug: string) => `/zboruri/${slug}`;
