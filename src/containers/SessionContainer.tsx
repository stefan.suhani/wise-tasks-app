import React from "react";
import { createContainer } from "unstated-next";

import { Me } from "@/domains/me";
import { useRouter } from "next/router";
import { storage } from "@/services";
import { getProfile, login, renewAccessToken } from "@/services/api";

export interface RegistrationOptions {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface LoginOptions {
  email: string;
  password: string;
}

const mapSession: (session: any) => Session = (session) => {
  return {
    accessToken: session["access_token"],
    refreshToken: session["refresh_token"],
    idToken: session["id_token"],
  };
};

enum ActionType {
  UpdateSession = "update-session",
  ClearSession = "clear-session",
  Error = "error",
}

interface LoggedInAction {
  type: ActionType.UpdateSession;
  me: Me;
  session: Session;
}

interface LoggedOutAction {
  type: ActionType.ClearSession;
}

interface ErrorAction {
  type: ActionType.Error;
  error: Error;
}

export type Action = LoggedInAction | LoggedOutAction | ErrorAction;

interface Session {
  accessToken: string;
  refreshToken: string;
  idToken: string;
}

export interface State {
  loading: boolean;
  session?: Session;
  error?: Error;
  me?: Me;
}

const reducer: React.Reducer<State, Action> = (state, action) => {
  switch (action.type) {
    case ActionType.UpdateSession: {
      const { me, session } = action;

      return { loading: false, me, session };
    }
    case ActionType.ClearSession:
      return { loading: false };
    case ActionType.Error:
      return { loading: false, error: action.error };
    default:
      return state;
  }
};

export const useSession = () => {
  const [state, dispatch] = React.useReducer(reducer, {
    loading: true,
    session: storage.getItem<Session>("session"),
  });
  const router = useRouter();

  const onSessionUpdated = (session?: Session) => () => {
    storage.setItem("session", session);
  };

  const clearSession = () => dispatch({ type: ActionType.ClearSession });

  React.useEffect(() => {
    onSessionUpdated(state.session)();
  }, [state.session]);

  const loadMe = React.useCallback(async () => {
    const session = state.session;

    if (!session) return;

    return getProfile()
      .then((me) => {
        dispatch({ type: ActionType.UpdateSession, me, session });

        return me;
      })
      .catch(() => clearSession());
  }, [state.session]);

  React.useEffect(() => {
    loadMe();
  }, [loadMe]);

  const renewToken = () => {
    if (!state.session || !state.session.refreshToken)
      return Promise.reject(new Error(`No session found. Signing out...`));

    return renewTokenAPI(state.session.refreshToken).catch(() => {
      clearSession();
      router.push("/login");
    });
  };

  const renewTokenAPI: (refreshToken: string) => Promise<Session> = (
    refreshToken
  ) => {
    return renewAccessToken(refreshToken).then(mapSession);
  };

  const loginWithEmailAndPassword = React.useCallback(
    (options: LoginOptions) => {
      return login(options)
        .then(mapSession)
        .then(({ accessToken, idToken, refreshToken }) => {
          dispatch({
            type: ActionType.UpdateSession,
            me: state.me,
            session: { accessToken, idToken, refreshToken },
          });

          onSessionUpdated({ accessToken, idToken, refreshToken })();
        });
    },
    []
  );

  const logout = React.useCallback(async () => {
    clearSession();
    return onSessionUpdated();
  }, []);

  return React.useMemo(
    () => ({
      loading: state.loading,
      error: state.error,
      me: state.me,
      authenticated: !!state.session,
      loadMe,
      logout,
      loginWithEmailAndPassword,
    }),
    [
      state.loading,
      state.error,
      state.me,
      state.session,
      logout,
      loadMe,
      loginWithEmailAndPassword,
    ]
  );
};

export const Session = createContainer(useSession);
