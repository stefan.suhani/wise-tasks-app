import "../styles/globals.css";
import type { AppProps } from "next/app";

import { Session } from "@/containers/SessionContainer";

import { LanguageContainer } from "@/components/LanguageContext";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <LanguageContainer.Provider>
      <Session.Provider>
        <Component {...pageProps} />
      </Session.Provider>
    </LanguageContainer.Provider>
  );
}

export default MyApp;
