import React from "react";
import * as yup from "yup";
import Link from "next/link";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { FormattedMessage, useIntl } from "react-intl";

import { translations } from "@/locales";
import { emailSchema, passwordSchema } from "@/schemas/Authorization";

import { Button } from "@/components/Button";
import { Form, FormField } from "@/components/Form";
import { useSession } from "@/containers/SessionContainer";
import { TextInputWithValidation } from "@/components/Input/TextInput";
import { storage } from "@/services";

const schema = yup.object({
  email: emailSchema,
  password: passwordSchema,
});

interface Props {}

const Login: NextPage<Props> = () => {
  const intl = useIntl();
  const router = useRouter();

  const { loginWithEmailAndPassword } = useSession();

  return (
    <main className="flex flex-col min-h-screen w-full relative items-center justify-center">
      <Form
        schema={schema}
        onSubmit={(values) => {
          loginWithEmailAndPassword(values).then(() => router.push("/tasks"));
        }}
        className="p-6 lg:max-w-xl w-full bg-white border-blue-100 border max-w-lg sm:rounded-3xl drop-shadow-lg"
      >
        {({ submitting }) => (
          <div className="flex flex-col w-full space-y-6">
            <div className="font-bold text-3xl mt-6 text-center text-blue-500">
              <FormattedMessage id={translations.pages.signIn.title} />
            </div>

            <FormField
              as={TextInputWithValidation}
              id="email"
              name="email"
              type="email"
              label={intl.formatMessage({
                id: translations.inputs.email.label,
              })}
            />

            <div className="relative">
              <FormField
                as={TextInputWithValidation}
                id="password"
                name="password"
                type="password"
                label={intl.formatMessage({
                  id: translations.inputs.password.label,
                })}
              />
            </div>

            <div className="h-full flex">
              <Button
                type="submit"
                appearance="blue"
                className="w-full"
                disabled={submitting}
              >
                <FormattedMessage id={translations.inputs.buttons.login} />
              </Button>
            </div>

            <div className="text-sm text-center mt-10 text-gray">
              <FormattedMessage
                id={translations.pages.signIn.noAccountSignUp}
                values={{
                  redirect: ((text: string) => (
                    <Link href="/register">
                      <span className="text-blue-500 cursor-pointer">
                        {text}
                      </span>
                    </Link>
                  )) as unknown as React.ReactNode,
                }}
              />
            </div>
          </div>
        )}
      </Form>
    </main>
  );
};

export default Login;
