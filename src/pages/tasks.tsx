import React from "react";
import { debounce } from "lodash";

import { NextPage } from "next";
import { FormattedMessage } from "react-intl";
import { PlusIcon } from "@heroicons/react/outline";

import { translations } from "@/locales";
import { Task, getTasks } from "@/services/api";

import { useOpen } from "@/hooks/useOpen";

import { Button } from "@/components/Button";
import { TaskCard } from "@/components/TaskCard";
import { TaskModal } from "@/components/TaskModal";
import { Header } from "@/components/Header/Header";
import { TextInput } from "@/components/Input/TextInput";

interface Props {}

const Tasks: NextPage<Props> = () => {
  const [query, setQuery] = React.useState("");
  const [tasks, setTasks] = React.useState<Task[]>([]);
  const [selectedTask, setSelectedTask] = React.useState<Task | undefined>();
  const taskModal = useOpen(false);

  const debouncedUpdate = debounce(setQuery, 300);

  const updateTaskList = () =>
    getTasks(query).then((response) => setTasks(response));

  React.useEffect(() => {
    updateTaskList();
  }, [query]);

  return (
    <main className="min-h-screen w-full relative">
      <Header />

      <div className="mt-20 p-6">
        <div className="bg-white rounded-3xl h-full overflow-hidden">
          <div className="flex border-b border-black/10 justify-between w-full space-x-6 p-6">
            <Button
              appearance="blue"
              className="h-auto px-4"
              onClick={() => {
                taskModal.open();
              }}
            >
              <PlusIcon className="h-6 w-6 mr-3" />
              <FormattedMessage id={translations.inputs.buttons.addTask} />
            </Button>

            <TextInput
              id="search"
              type="text"
              autoComplete="off"
              onChange={(event) => debouncedUpdate(event.target.value)}
              className="max-w-lg"
              placeholder="Search by title"
            />
          </div>

          <div className="flex flex-col p-6 space-y-6">
            {tasks.length > 0 ? (
              tasks.map((task) => (
                <TaskCard
                  onClick={() => {
                    setSelectedTask(task);
                    taskModal.open();
                  }}
                  id={task.id}
                  key={task.id}
                  onDelete={() => updateTaskList()}
                  title={task.title}
                  createDate={task.createDate}
                  description={task.description}
                />
              ))
            ) : (
              <div className="">
                There`s nothing here yet, but there will be soon
              </div>
            )}
          </div>
        </div>
      </div>

      <TaskModal
        task={selectedTask}
        isOpen={taskModal.isOpen}
        onClose={() => {
          taskModal.close();
          setSelectedTask(undefined);
          updateTaskList();
        }}
      />
    </main>
  );
};

export default Tasks;
