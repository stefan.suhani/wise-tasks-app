import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />

        <link
          href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap"
          rel="stylesheet"
        />

        <link
          href="https://fonts.googleapis.com/css2?family=Inter&family=Sora:wght@100;200;300;400;500;600;700;800&display=swap"
          rel="stylesheet"
        />

        <title>Tasks App WiseSystems</title>
        <meta name="description" content="Tasks App WiseSystems" />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <body className="text-black bg-slate-200">
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
