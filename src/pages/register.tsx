import React from "react";
import * as yup from "yup";
import Link from "next/link";
import { NextPage } from "next";
import { FormattedMessage, useIntl } from "react-intl";

import { translations } from "@/locales";
import {
  emailSchema,
  firstNameSchema,
  lastNameSchema,
  passwordSchema,
} from "@/schemas/Authorization";

import { Button } from "@/components/Button";
import { Form, FormField } from "@/components/Form";
import { useSession } from "@/containers/SessionContainer";
import { TextInputWithValidation } from "@/components/Input/TextInput";
import { register } from "@/services/api";
import { useRouter } from "next/router";

const schema = yup.object({
  email: emailSchema,
  firstName: firstNameSchema,
  lastName: lastNameSchema,
  password: passwordSchema,
});

interface Props {}

const Home: NextPage<Props> = () => {
  const intl = useIntl();
  const session = useSession();
  const router = useRouter();

  return (
    <main className="flex flex-col min-h-screen w-full relative items-center justify-center">
      <Form
        schema={schema}
        onSubmit={(data) => {
          register(data).then((response) =>
            session
              .loginWithEmailAndPassword({
                email: data.email,
                password: data.password,
              })
              .then(() => router.push("/login"))
          );
        }}
        className="p-6 lg:max-w-xl w-full bg-white border-blue-100 border max-w-lg sm:rounded-3xl drop-shadow-lg"
      >
        {({ submitting }) => {
          return (
            <div className="flex flex-col w-full space-y-6">
              <div className="font-bold text-3xl text-center mt-6 text-blue-500">
                <FormattedMessage id={translations.pages.signUp.title} />
              </div>

              <FormField
                as={TextInputWithValidation}
                id="firstName"
                name="firstName"
                type="text"
                placeholder={intl.formatMessage({
                  id: translations.inputs.firstName.placeholder,
                })}
                label={intl.formatMessage({
                  id: translations.inputs.firstName.label,
                })}
              />

              <FormField
                as={TextInputWithValidation}
                id="lastName"
                name="lastName"
                type="text"
                placeholder={intl.formatMessage({
                  id: translations.inputs.lastName.placeholder,
                })}
                label={intl.formatMessage({
                  id: translations.inputs.lastName.label,
                })}
              />

              <FormField
                as={TextInputWithValidation}
                id="email"
                name="email"
                type="email"
                placeholder={intl.formatMessage({
                  id: translations.inputs.email.placeholder,
                })}
                label={intl.formatMessage({
                  id: translations.inputs.email.label,
                })}
              />

              <FormField
                as={TextInputWithValidation}
                id="password"
                name="password"
                type="password"
                placeholder={intl.formatMessage({
                  id: translations.inputs.password.placeholder,
                })}
                label={intl.formatMessage({
                  id: translations.inputs.password.label,
                })}
              />

              <div className="h-full flex">
                <Button
                  appearance="blue"
                  className="w-full"
                  disabled={submitting}
                >
                  <FormattedMessage
                    id={translations.inputs.buttons.createAccount}
                  />
                </Button>
              </div>

              <div className="text-sm text-center mt-10 text-gray">
                <FormattedMessage
                  id={translations.pages.signUp.haveAccountSignIn}
                  values={{
                    redirect: ((text: string) => (
                      <Link href="/login">
                        <span className="text-blue-500 cursor-pointer">
                          {text}
                        </span>
                      </Link>
                    )) as unknown as React.ReactNode,
                  }}
                />
              </div>
            </div>
          );
        }}
      </Form>
    </main>
  );
};

export default Home;
