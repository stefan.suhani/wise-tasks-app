import React from "react";
import { Button } from "./Button";
import { TrashIcon } from "@heroicons/react/outline";
import { FormattedDate } from "react-intl";
import { deleteTask } from "@/services/api";

interface Props {
  id: string;
  title: string;
  createDate: Date;
  description: string;
  onDelete(): void;
  onClick(): void;
}

export const TaskCard: React.FC<Props> = ({
  id,
  title,
  createDate,
  description,
  onClick,
  onDelete,
}) => {
  return (
    <div
      className="p-6 bg-blue-100 hover:bg-blue-200 transition-colors text-black rounded-3xl"
      {...{ onClick }}
    >
      <div className="flex justify-between items-center">
        <div className="text-xl max-w-lg truncate">{title}</div>

        <div className="flex items-center space-x-6">
          <div className="text-lg text-gray max-w-lg truncate">
            <FormattedDate
              value={createDate}
              day="2-digit"
              month="long"
              year="numeric"
            />
          </div>

          <Button
            appearance="red"
            className="h-10 w-10"
            onClick={(e) => {
              e.stopPropagation();
              deleteTask(id).then(onDelete);
            }}
          >
            <TrashIcon className="h-6 w-6" />
          </Button>
        </div>
      </div>

      <div className="font-inter mt-4">{description}</div>
    </div>
  );
};
