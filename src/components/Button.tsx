import React from "react";
import classnames from "classnames";

import { Is, IsComponent, IsProps } from "@/components/Is";

export type Appearance = "none" | "pink" | "hollow" | "blue" | "red";

const styles: Record<Appearance, string> = {
  none: "disabled:cursor-default",
  blue: "bg-blue-500 rounded-lg text-white hover:bg-blue-600 transition-colors",
  red: "bg-red-500 rounded-lg text-white hover:bg-red-600 transition-colors",
  pink: "bg-pink-accent rounded-full text-white hover:bg-pink-hover transition-colors",
  hollow:
    "border border-solid border-pink-accent bg-none hover:bg-white text-pink-accent rounded-full",
};

interface Props {
  appearance?: Appearance;
  fat?: boolean;
}

export const Button = React.forwardRef(
  <T extends IsComponent>(
    {
      // @ts-ignore
      is = "button",
      appearance = "none",
      fat = false,
      disabled,
      className,
      children,
      onClick,
      ...rest
    }: Props & IsProps<T>,
    ref: React.Ref<HTMLElement>
  ) => (
    <Is
      {...{ ref, is }}
      className={classnames(
        `${appearance !== "none" && fat ? "h-[72px]" : "h-[60px]"}`,
        styles[appearance],
        className,
        {
          "group relative flex items-center justify-center transition-colors cursor-pointer focus:outline-none":
            appearance,
        }
      )}
      disabled={disabled}
      onClick={disabled ? null : onClick}
      {...rest}
    >
      {children}
    </Is>
  )
);
