import React from "react";
import Link from "next/link";
import Image from "next/image";
import classnames from "classnames";

import { useSession } from "@/containers/SessionContainer";

export const Header: React.FC = () => {
  const { me, loading } = useSession();

  return (
    !loading && (
      <div
        id="headerContainer"
        className={classnames(
          "flex fixed inset-x-0 top-0 bg-white justify-center z-40 h-20 px-10 text-black shadow transition-colors duration-300"
        )}
      >
        <div className="flex items-center w-full justify-between transition-colors duration-300 max-w-[1440px]">
          <div className="text-blue-500 font-bold text-xl">Tasks App</div>

          <div className="">{`${me.firstName} ${me.lastName}`}</div>
        </div>
      </div>
    )
  );
};
