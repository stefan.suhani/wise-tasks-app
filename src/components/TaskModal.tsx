import React from "react";
import * as yup from "yup";

import { FormattedMessage, useIntl } from "react-intl";
import { Dialog, Transition } from "@headlessui/react";

import { Task, createTask, updateTask } from "@/services/api";
import { translations } from "@/locales";

import { Form, FormField } from "./Form";
import { TextInputWithValidation } from "./Input/TextInput";
import { Button } from "./Button";
import { TextAreaInputWithValidation } from "./Input/TextAreaInput";

import { taskDescriptionSchema, taskTitleSchema } from "@/schemas/Tasks";

interface Props {
  task?: Task;
  isOpen: boolean;
  onClose(): void;
}

const schema = yup.object({
  title: taskTitleSchema,
  description: taskDescriptionSchema,
});

export const TaskModal: React.FC<Props> = ({ task, isOpen, onClose }) => {
  const intl = useIntl();
  return (
    <Transition appear show={isOpen} as={React.Fragment}>
      <Dialog as="div" className="relative z-10" onClose={onClose}>
        <Transition.Child
          as={React.Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center">
            <Transition.Child
              as={React.Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full rounded-3xl transform overflow-hidden bg-white shadow-xl max-w-lg transition-all p-6">
                <Form
                  initialValues={
                    task && { title: task.title, description: task.description }
                  }
                  schema={schema}
                  onSubmit={(values) => {
                    (task
                      ? updateTask(task.id, values.title, values.description)
                      : createTask(values.title, values.description)
                    ).then(onClose);
                  }}
                >
                  {({ submitting }) => (
                    <div className="flex flex-col w-full space-y-6">
                      <div className="font-bold text-3xl mt-6 text-center text-blue-500">
                        <FormattedMessage
                          id={
                            task
                              ? translations.modals.task.editTitle
                              : translations.modals.task.createTitle
                          }
                        />
                      </div>

                      <FormField
                        as={TextInputWithValidation}
                        id="title"
                        name="title"
                        type="text"
                        label={intl.formatMessage({
                          id: translations.inputs.taskTitle.label,
                        })}
                        placeholder={intl.formatMessage({
                          id: translations.inputs.taskTitle.placeholder,
                        })}
                      />

                      <FormField
                        as={TextAreaInputWithValidation}
                        id="description"
                        name="description"
                        type="text"
                        className="h-60"
                        label={intl.formatMessage({
                          id: translations.inputs.taskDescription.label,
                        })}
                        placeholder={intl.formatMessage({
                          id: translations.inputs.taskDescription.placeholder,
                        })}
                      />

                      <Button
                        type="submit"
                        appearance="blue"
                        className="w-full"
                        disabled={submitting}
                      >
                        <FormattedMessage
                          id={
                            task
                              ? translations.inputs.buttons.editTask
                              : translations.inputs.buttons.createTask
                          }
                        />
                      </Button>
                    </div>
                  )}
                </Form>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};
