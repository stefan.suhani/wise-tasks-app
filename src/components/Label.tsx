import React from 'react';
import classnames from 'classnames';

export interface Props extends React.DetailedHTMLProps<React.LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement> {}

export const Label = ({ className, ...props }: React.PropsWithChildren<Props>) => (
  <label {...props} className={classnames('text-base font-bold inline-block pb-1', className)} />
);
