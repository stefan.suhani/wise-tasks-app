import {
  LoginOptions,
  RegistrationOptions,
} from "@/containers/SessionContainer";
import { Me } from "@/domains/me";
import { faker } from "@faker-js/faker";
import { storage } from "./storage";

export const register = (data: RegistrationOptions) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      storage.setItem("profile", data);
      resolve(data);
    }, 300);
  });

export const getProfile = () =>
  new Promise<Me>((resolve, reject) => {
    setTimeout(() => {
      const profileData = storage.getItem<RegistrationOptions>("profile");

      resolve(profileData ? profileData : undefined);
    }, 300);
  });

export const login = (data: LoginOptions) =>
  new Promise<{ accessToken: string; refreshToken: string; idToken: string }>(
    (resolve, reject) => {
      setTimeout(() => {
        const profileData = storage.getItem<RegistrationOptions>("profile");

        if (
          data.email === profileData.email &&
          data.password === profileData.password
        )
          resolve({
            accessToken: "acceess",
            refreshToken: "refresh",
            idToken: "id",
          });
      }, 300);
    }
  );

export const renewAccessToken = (refreshToken) =>
  new Promise<{ accessToken: string; refreshToken: string; idToken: string }>(
    (resolve, reject) => {
      setTimeout(() => {
        resolve({
          accessToken: "acceess",
          refreshToken: "refresh",
          idToken: "id",
        });
      }, 300);
    }
  );

export interface Task {
  id: string;
  title: string;
  description: string;
  createDate: Date;
}

export const getTasks = (query: string) =>
  new Promise<Task[]>((resolve, reject) => {
    setTimeout(() => {
      const tasks = storage.getItem<Task[]>("tasks");

      if (tasks === undefined) storage.setItem("tasks", []);

      const result = storage
        .getItem<Task[]>("tasks")
        .filter((item) => item.title.includes(query))
        .sort((a, b) => {
          return (
            new Date(a.createDate).getTime() - new Date(b.createDate).getTime()
          );
        });

      resolve(result);
    }, 300);
  });

export const createTask = (title: string, description: string) =>
  new Promise<Task>((resolve, reject) => {
    setTimeout(() => {
      const tasks = storage.getItem<Task[]>("tasks");

      const newTask = {
        id: faker.datatype.uuid(),
        title: title,
        description: description,
        createDate: new Date(),
      };

      tasks.push(newTask);

      storage.setItem("tasks", tasks);

      resolve(newTask);
    }, 300);
  });

export const updateTask = (id: string, title: string, description: string) =>
  new Promise<Task>((resolve, reject) => {
    setTimeout(() => {
      const tasks = storage.getItem<Task[]>("tasks");

      const index = tasks.findIndex((item) => item.id === id);

      tasks[index] = { ...tasks[index], title, description };

      storage.setItem("tasks", tasks);

      resolve(tasks[index]);
    }, 300);
  });

export const deleteTask = (id: string) =>
  new Promise<number>((resolve, reject) => {
    setTimeout(() => {
      const tasks = storage.getItem<Task[]>("tasks");

      tasks.splice(
        tasks.findIndex((item) => item.id === id),
        1
      );

      storage.setItem("tasks", tasks);

      resolve(1);
    }, 300);
  });
