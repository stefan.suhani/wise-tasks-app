import * as yup from "yup";

import { translations } from "@/locales";

export const emailSchema = yup
  .string()
  .email()
  .max(128)
  .label(translations.inputs.email.label)
  .required();

export const passwordSchema = yup
  .string()
  .min(8)
  .max(128)
  .label(translations.inputs.password.label)
  .required();

export const firstNameSchema = yup
  .string()
  .max(128)
  .label(translations.inputs.firstName.label)
  .required();

export const lastNameSchema = yup
  .string()
  .max(128)
  .label(translations.inputs.lastName.label)
  .required();
