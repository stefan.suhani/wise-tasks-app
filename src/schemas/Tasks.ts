import * as yup from "yup";

import { translations } from "@/locales";

export const taskTitleSchema = yup
  .string()
  .min(2)
  .max(128)
  .label(translations.inputs.taskTitle.label)
  .required();

export const taskDescriptionSchema = yup
  .string()
  .min(2)
  .max(1024)
  .label(translations.inputs.taskDescription.label)
  .required();
