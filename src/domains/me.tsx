export interface Me {
  firstName: string;
  lastName: string;
  email: string;
}
