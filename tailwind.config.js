/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}", "../common/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      amatic: ["Amatic SC", "cursive"],
      sora: ["Sora", "sans-serif"],
      inter: ["Inter", "sans-serif"],
    },
    screens: {
      xs: "480px",
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      "2xl": "1536px",
    },
    extend: {
      fontSize: {
        huge: "6rem",
      },
      aspectRatio: {
        "1/2": "1 / 2",
        "2/1": "2 / 1",
        "4/3": "4 / 3",
        "3/4": "3 / 4",
      },
      colors: {
        gray: {
          DEFAULT: "#676767",
        },
        black: "#0A0A38",
        pink: {
          accent: "#FF71A2",
          hover: "#FF528E",
        },
      },
    },
  },
  plugins: [],
};
